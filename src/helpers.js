const fs = require('fs')

const mainFileContent = name => `import React from 'react'
import PropTypes from 'prop-types'
import './${name}.scss'

export const ${name} = ({

}) => {
  return (
    <div>

    </div>
  )
}

${name}.propTypes = {

}
`

const indexFileContent = name => `export { ${name} } from './${name}'\n`

const componentsIndexFileContent = names => (
  names.map(n => `export { ${n} } from './${n}'\n`).join('')
)

const componentsPath = 'created_components'

const getFilePath = name => `${process.cwd()}/${componentsPath}/${name}`

const getFilesData = (name, innerName) => {
  const pathPiece = innerName ? `components/${innerName}` : ''
  return [
    {
      name: getFilePath(`${name}/${pathPiece}/${innerName || name}.js`),
      content: mainFileContent(innerName || name),
    },
    {
      name: getFilePath(`${name}/${pathPiece}/${innerName || name}.scss`),
    },
    {
      name: getFilePath(`${name}/${pathPiece}/index.js`),
      content: indexFileContent(innerName || name),
    },
  ]
}

const createDir = (name, recursive = true) => {
  fs.mkdirSync(getFilePath(name), { recursive })
}

const createFile = file => {
  fs.writeFileSync(file.name, file.content || '')
}

module.exports = {
  indexFileContent,
  mainFileContent,
  getFilesData,
  getFilePath,
  componentsIndexFileContent,
  createDir,
  createFile,
}
