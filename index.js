const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { createProxyMiddleware } = require('http-proxy-middleware')
const config = require('./config')
const {
  getFilePath,
  getFilesData,
  componentsIndexFileContent,
  createDir,
  createFile,
} = require('./src/helpers')

const jsonParser = bodyParser.json()

const app = express()
app.use(cors())

// Emulate errors
// app.put('*', function(req, res) {
//   res.send(function(err) {
//     res.status(500).send('Error occurred')
//   })
// })

config.forEach(item => {
  const { endpoint, ...middlewareParams } = item
  app.use(endpoint, createProxyMiddleware({ ...middlewareParams }))
})

app.post('/create-component-files', jsonParser, (req, res) => {
  const { name, components } = req.body
  try {
    createDir(name)
    const fileData = getFilesData(name)
    fileData.forEach(createFile)
    if (components) {
      createDir(`${name}/components`)
      components.forEach(c => {
        createDir(`${name}/components/${c}`)
        const fileData = getFilesData(name, c)
        fileData.forEach(createFile)
      })
      createFile({
        name: getFilePath(`${name}/components/index.js`),
        content: componentsIndexFileContent(components),
      })
    }
    res.send({
      code: res.statusCode,
      message: `Structure for '${name}' was successfully created!`,
    })
  } catch (err) {
    res.status(500)
    res.send({
      code: res.statusCode,
      message: `Structure for '${name}' was not created! ${err}`,
    })
  }
})

app.use((req, res, next) => {
  res.status(404).json({
    code: res.statusCode,
    message: 'Endpoint does not exist.'
  })
})

const server = app.listen(9000)
server.setTimeout(3000)
