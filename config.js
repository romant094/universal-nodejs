module.exports = [
  {
    endpoint: '/crm',
    // target: 'https://4ed7df7af193.ngrok.io/',
    target: 'https://crm-development.innoscripta.com',
    // target: 'https://crm-testing.innoscripta.com',
    // target: 'http://localhost:8080',
    // target: 'http://backend-feature-cu-kk6unw.qa-crm.innoscripta.com/',
    changeOrigin: true,
    pathRewrite: {
      '^/crm': '/',
    },
  },
  {
    endpoint: '/auth',
    target: 'https://auth-api-testing.innoscripta.com',
    changeOrigin: true,
    pathRewrite: {
      '^/auth': '/',
    },
  },
  {
    endpoint: '/searcher',
    target: 'http://35.156.169.221:8000/api',
    changeOrigin: true,
    pathRewrite: {
      '^/searcher': '/',
    },
  },
  {
    endpoint: '/campaign',
    target: 'http://3.126.250.106:8000/api',
    changeOrigin: true,
    pathRewrite: {
      '^/campaign': '/',
    },
  },
  {
    endpoint: '/files',
    target: 'http://18.184.63.93:4000/api/v1',
    changeOrigin: true,
    pathRewrite: {
      '^/files': '/',
    },
  },
  {
    endpoint: '/accounting/',
    target: 'https://accounting-testing.innoscripta.com',
    // target: 'https://accounting.innoscripta.com',
    changeOrigin: true,
    pathRewrite: {
      '^/accounting': '/',
    },
  },
  {
    endpoint: '/fragen',
    target: 'https://fragenliste-api-v2-testing.innoscripta.com/',
    // target: 'http://fragenliste-feature-cu-hv9q4y.qa.innoscripta.com',
    changeOrigin: true,
    pathRewrite: {
      '^/fragen': '/',
    },
  },
  {
    endpoint: '/fvs',
    // target: 'https://api-testing.innoscripta.com/fvs/',
    target: 'https://api-testing.innoscripta.com/fvs/',
    changeOrigin: true,
    pathRewrite: {
      '^/fvs': '/',
    },
  },
  {
    endpoint: '/ims',
    // target: 'https://api-testing.innoscripta.com/ims',
    target: 'https://ims-testing.innoscripta.com/',
    changeOrigin: true,
    pathRewrite: {
      '^/ims': '/',
    },
  },
]
