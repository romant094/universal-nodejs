## Create component structure
### Description
Creates a structure ready to be copy-pasted to the React App.

`http://localhost:9000/create-component-files`

### Body
`name`: desirable name of the component
`components`: array of strings; if the component will have nesting components just add names into the array

#### Example
Request:
```curl
curl --location --request POST 'http://localhost:9000/create-component-files' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Employees",
    "components": ["EmployeeUtilization", "EmployeeModal"]
}'
```
![img.png](docs/images/example.png)
Result:<br />
![img.png](docs/images/structure.png)
